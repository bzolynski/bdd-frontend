<?php

use Behat\MinkExtension\Context\MinkContext;
use Behat\Mink\Exception\ExpectationException;

class BaseContext extends MinkContext
{
    /**
     * Spin method to run the assertion multiple times before failing, useful for AJAX calls
     * Also usable when waiting for the element to show up
     *
     * Usage:
     * $this->spins(function() use ($text) {
     *     $this->assertSession()->pageTextContains($text);
     * });
     *
     * @param $closure
     * @param int $tries
     * @throws Exception
     */
    public function spins($closure, $tries = 10)
    {
        for ($i = 0; $i <= $tries; $i++) {
            try {
                $closure();
                return;
            } catch (\Exception $e) {
                if ($i == $tries) {
                    throw $e;
                }
            }
            sleep(1);
        }
    }

    /**
     * @param string $message
     * @throws ExpectationException
     */
    public function throwExpectationException($message)
    {
        throw new ExpectationException($message, $this->getSession());
    }

    /**
     * @param string $expectedTitle
     * @throws ExpectationException
     */
    public function assertPageTitleEquals($expectedTitle)
    {
        $pageTitle = $this->getSession()->getPage()->find('css', 'title')->getHtml();
        if ($expectedTitle !== $pageTitle) {
            $this->throwExpectationException('Page title not matched');
        }
    }

    /**
     * Assert that element do not contains "hidden" class
     *
     * @param string $css
     * @param string $selectorType
     * @param string $selectorName
     *
     * @throws ExpectationException
     */
    public function assertElementNotHidden($css, $selectorType, $selectorName)
    {
        $element = $this->getSession()->getPage()->find(
            'xpath', "//{$css}[@{$selectorType}='{$selectorName}' and not(contains(@class, 'hidden'))]"
        );
        if (!$element) {
            $this->throwExpectationException('Could not find the element');
        }
    }
}
