<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;

/**
 * Defines application features from the specific context.
 */
class UserInterfaceContext extends BaseContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given I am on the entry page
     */
    public function iAmOnTheEntryPage()
    {
        $url = '/expense-entry';
        $this->getSession()->visit($this->locatePath($url));
        if ($this->getSession()->getCurrentUrl() !== $this->locatePath($url)) {
            $this->throwExpectationException('Url not found');
        }
        $this->assertPageTitleEquals('BDD test - Expense entry');
    }

    /**
     * @When I enter expense value
     */
    public function iEnterExpenseValue()
    {
        $element = $this->getSession()->getPage()->find('xpath', "//input[@id='expense_value']");
        if (!$element) {
            $this->throwExpectationException('Could not find the element');
        }
        $element->setValue(100);
    }

    /**
     * @Then I should be able to continue
     */
    public function iShouldBeAbleToContinue()
    {
        $this->assertFieldContains('expense_value', 100);
        $this->assertElementNotHidden('button', 'id', 'continue_button');
    }
}
