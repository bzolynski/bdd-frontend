Feature: Enter expense value
  In order to set expense value
  As an user
  I need to be able to enter expense value

  @javascript
  Scenario: Adding an expense value
    Given I am on the entry page
    When I enter expense value
    Then I should be able to continue
