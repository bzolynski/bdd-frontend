Installation
============

You can install whole environment using Docker containers:

- selenium/hub
- selenium/node-firefox

You can also use Vagrant, or install it manually on your local machine if you wish.

1. Get Selenium Server and put here or in other directory:

        sudo wget http://selenium-release.storage.googleapis.com/2.52/selenium-server-standalone-2.52.0.jar

2. Install xvfb:

        sudo apt-get install xvfb -y

3. Install Java (to run Selenium Server).

        sudo apt-get install openjdk-7-jre-headless -y

Other installments:
-------------------

1. Install PHP:

        sudo apt-get install php5-cli php5-curl

2. Install composer:

        sudo apt-get install curl git
        curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

3. Install nginx:

        sudo apt-get install nginx

    Create file `/etc/nginx/sites-available/bdd.localhost.conf` and put server configuration there:
    
        server {
                listen 80;
                root <your path>/bdd/public;
        
                server_name bdd.localhost;
        
                location @rewrite{
                      rewrite ^(.*)$ /index.php?url=$1;
                }
        
                location / {
                        try_files $uri $uri @rewrite;
                        fastcgi_split_path_info ^(.+\.php)(/.+)$;
                        fastcgi_pass unix:/var/run/php5-fpm.sock;
                        fastcgi_index index.php;
                        include fastcgi_params;
                }
        
                location ~ \.css {
                        add_header  Content-Type    text/css;
                }
        
                location ~ \.js {
                        add_header  Content-Type    application/x-javascript;
                }
        }
        
    Link file to `/etc/nginx/sites-enabled/` folder:
    
        sudo ln -s /etc/nginx/sites-available/bdd.localhost.conf /etc/nginx/sites-enabled/bdd.localhost.conf
        
    Add host to /etc/hosts:
    
        127.0.0.1 bdd.localhost
    
4. Install php5-fpm:

        sudo apt-get install php5-fpm
    
Init run
========

1. Run Selenium Server:

        DISPLAY=:1 xvfb-run java -jar selenium-server-standalone-2.52.0.jar

2. Run nginx if not running:

        sudo service nginx start

    We can make sure that our web server will restart automatically when the server is rebooted by typing:
        
        sudo update-rc.d nginx defaults

3. Run php5-fpm:

        sudo service php5-fpm start

4. Run composer install:

        composer install
        
Usage
=====

1. Running Behat:
    
        vendor/bin/behat

2. Running Behat with tags filters:

        vendor/bin/behat --tags '@javascript'
        
3. Appending snippets to context class:

        vendor/bin/behat --dry-run --append-snippets

