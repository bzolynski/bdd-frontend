<?php

require_once '../vendor/autoload.php';

$paths = array(
    '' => 'index.html',
    'expense-entry' => 'expense-entry.html',
    'done' => 'done.html'
);

$url = $_GET['url'];

$router = new Router($paths);
$router->dispatch($url);
