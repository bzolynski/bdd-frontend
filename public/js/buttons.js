$(document).ready(function() {
    $('#expense_value').on('input', function() {
        var value = $(this).val();
        if (value !== 0 && value !== '') {
            $('#continue_button').removeClass('hidden');
        } else {
            $('#continue_button').addClass('hidden');
        }
    });
});
