<?php

class Router
{
    private $paths = array();

    public function __construct(array $paths)
    {
        $this->paths = $paths;
    }

    /**
     * @param string $url
     *
     * @return void
     */
    public function dispatch($url)
    {
        $url = trim($url, '/');
        if (array_key_exists($url, $this->paths)) {
            echo file_get_contents('../views/' . $this->paths[$url]);
            return;
        }
        $this->render404();
    }

    /**
     * @return void
     */
    public function render404()
    {
        header("HTTP/1.0 404 Not Found");
        print '<h1>404 Page not found</h1>';
    }
}
